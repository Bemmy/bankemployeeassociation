package bank;

public class BankEmployeeSimulation {

    
    public static void main(String[] args) {
        
        Bank bank = new Bank("TD");
        Employee employee = new Employee("Fredrick");
        
        System.out.println(employee.getName() + " is an employee of " +
                bank.getName());
    }
}
